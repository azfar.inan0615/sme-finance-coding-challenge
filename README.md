1. Please make sure that JAVA_HOME and MAVEN_HOME path are set in environment variables
 (Install java version 1.8 and maven 3.8)
2. Make sure postgres service is running on port 5432
 (If postgres is not downloaded follow this instruction - https://www.2ndquadrant.com/en/blog/pginstaller-install-postgresql/ )
3. clone this repo
4. go to project directory
5. mvn clean install
6. java -jar target/todo-list-0.0.1-SNAPSHOT.jar



