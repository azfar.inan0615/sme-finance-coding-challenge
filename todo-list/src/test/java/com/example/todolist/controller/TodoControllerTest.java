package com.example.todolist.controller;

import com.example.todolist.model.Todo;
import com.example.todolist.service.TodoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TodoController.class)
public class TodoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    TodoService todoService;

    private static final String SAVE_TASK_REQUEST_STR = "{\"task\":\"Sleep\",\"priority\":\"high\"}";
    private static final String SAVE_TASK_FAILURE_REQUEST_STR = "{\"task\":\"Sleep\",\"priority\":\"no\"}";

    Todo todoEntity = Todo.builder()
            .id(Long.valueOf(2))
            .task("Learn Java")
            .priority("high")
            .status("pending")
            .weight(1)
            .build();

    Todo todoFailureEntity = Todo.builder()
            .id(Long.valueOf(4))
            .task("Learn Java")
            .priority("random")
            .status("pending")
            .weight(1)
            .build();

    List<Todo> todoListResponse = new ArrayList<Todo>() {{
        Arrays.asList(todoEntity);
    }};

    Map<String, List<Todo>> map = new HashMap<>();

    ResponseEntity<Todo> save_response = new ResponseEntity<>(todoEntity, HttpStatus.CREATED);
    ResponseEntity<Todo> save_update_response = new ResponseEntity<>(todoEntity, HttpStatus.OK);
    ResponseEntity<Todo> save_failure_response = new ResponseEntity<>(todoFailureEntity, HttpStatus.NOT_ACCEPTABLE);

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void should_get_all_todo_list_test() throws Exception{
        map.put("data", todoListResponse);
        when(todoService.getAllTodoList("pending")).thenReturn(new ResponseEntity<>(map, HttpStatus.OK));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/todo"))
                .andExpect(status().isOk());
    }

    @Test
    public void should_get_empty_list_test() throws Exception{
        when(todoService.getAllTodoList("pending")).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/todo?status=pending"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void should_get_internel_server_error_test() throws Exception{
        when(todoService.getAllTodoList("pending")).thenReturn(new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/todo?status=pending"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void should_successfully_create_task_test() throws Exception {
        when(todoService.createTask(ArgumentMatchers.any(Todo.class))).thenReturn(save_response);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(SAVE_TASK_REQUEST_STR))
                .andExpect(status().isCreated());
    }

    @Test
    public void should_fail_to_create_task_test() throws Exception {
        when(todoService.createTask(ArgumentMatchers.any(Todo.class))).thenReturn(save_failure_response);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(SAVE_TASK_FAILURE_REQUEST_STR))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void should_update_task_test() throws Exception {
        when(todoService.updateTask(Mockito.any(), Mockito.any())).thenReturn(save_update_response);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/todo/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(SAVE_TASK_REQUEST_STR))
                .andExpect(status().isOk());
    }

    @Test
    public void should_be_able_to_delete_task_test() throws Exception {
        when(todoService.deleteTask(2)).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/todo/2"))
                .andExpect(status().isNoContent());
    }

}
