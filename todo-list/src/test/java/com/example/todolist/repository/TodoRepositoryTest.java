package com.example.todolist.repository;

import com.example.todolist.model.Todo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TodoRepositoryTest {

    @Autowired
    TodoRepository todoRepository;

    @Autowired
    private TestEntityManager entityManager;

    Todo todoEntity = Todo.builder()
            .task("Please sleep")
            .priority("low")
            .weight(3)
            .status("pending")
            .build();

    @Test
    public void should_find_no_todo_list_if_repository_is_empty() {
        List<Todo> todo = todoRepository.findAll();
        assertThat(todo).isEmpty();
    }

    @Test
    public void should_store_a_task_in_todo_list() {
        Todo todo = todoRepository.save(new Todo("GTS", "high", "pending", 1));

        assertThat(todo).hasFieldOrPropertyWithValue("task", "GTS");
        assertThat(todo).hasFieldOrPropertyWithValue("priority", "high");
        assertThat(todo).hasFieldOrPropertyWithValue("weight", 1);
    }

    @Test
    public void should_find_all_todo_list() {
        Todo todo1 = new Todo("GTS", "high", "pending", 1);
        entityManager.persist(todo1);

        Todo todo2 = new Todo("Go To Sleep", "medium", "pending", 2);
        entityManager.persist(todo2);

        List<Todo> todoList = todoRepository.findAll();

        assertThat(todoList).hasSize(2).contains(todo1, todo2);
    }

    @Test
    public void should_find_task_by_id() {
        Todo todo1 = new Todo("Sleep", "high", "done", 1);
        entityManager.persist(todo1);

        Todo todo2 = new Todo("Wake Up", "low", "done", 3);
        entityManager.persist(todo2);

        Todo foundTodo = todoRepository.findById(todo2.getId()).get();

        assertThat(foundTodo).isEqualTo(todo2);
    }

    @Test
    public void should_find_all_pending_tasks() {
        Todo todo1 = new Todo("Sleep today", "high", "pending", 1);
        entityManager.persist(todo1);

        Todo todo2 = new Todo("Work today", "high", "done", 1);
        entityManager.persist(todo2);

        Todo todo3 = new Todo("Go to work tomorrow", "low", "pending", 3);
        entityManager.persist(todo3);

        List<Todo> todoList = todoRepository.findAllByStatus("pending");

        assertThat(todoList).hasSize(2).contains(todo1, todo3);
    }

    @Test
    public void should_update_task_by_id() {
        Todo todo1 = new Todo("Sleep today", "high", "pending", 1);
        entityManager.persist(todo1);

        Todo todo2 = new Todo("Wake up", "medium", "pending", 2);
        entityManager.persist(todo2);

        Todo updatedTodo = new Todo("Sleep Must", "high", "pending", 1);

        Todo todoEntity = todoRepository.findById(todo2.getId()).get();
        todoEntity.setTask(updatedTodo.getTask());
        todoEntity.setPriority(updatedTodo.getPriority());
        todoEntity.setStatus(updatedTodo.getStatus());
        todoEntity.setWeight(updatedTodo.getWeight());
        todoRepository.save(todoEntity);

        Todo checkTodo = todoRepository.findById(todo2.getId()).get();

        assertThat(checkTodo.getId()).isEqualTo(todo2.getId());
        assertThat(checkTodo.getTask()).isEqualTo(updatedTodo.getTask());
        assertThat(checkTodo.getPriority()).isEqualTo(updatedTodo.getPriority());
        assertThat(checkTodo.getWeight()).isEqualTo(updatedTodo.getWeight());
        assertThat(checkTodo.getStatus()).isEqualTo(updatedTodo.getStatus());
    }

    @Test
    public void should_delete_task_by_id() {
        Todo todo1 = new Todo("Sleep", "high", "pending", 1);
        entityManager.persist(todo1);

        Todo todo2 = new Todo("Dont panic", "medium", "done", 2);
        entityManager.persist(todo2);

        todoRepository.deleteById(todo1.getId());

        List<Todo> todoList = todoRepository.findAll();

        assertThat(todoList).hasSize(1).contains(todo2);
    }
}
