package com.example.todolist.service;

import com.example.todolist.model.Todo;
import com.example.todolist.repository.TodoRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TodoServiceTest {

    @Mock
    TodoRepository todoRepository;

    @InjectMocks
    TodoService todoService;

    private static final Todo todoEntity = Todo.builder()
            .id(Long.valueOf(2))
            .task("Go to sleep")
            .priority("low")
            .status("pending")
            .weight(3)
            .build();

    private static final Todo fakeTodoEntity = Todo.builder()
            .task("Go to sleep")
            .priority("random")
            .build();

    private static final String status = "pending";

    @Test
    public void get_success_when_create_task_test() {
        when(todoRepository.save(ArgumentMatchers.any(Todo.class))).thenReturn(todoEntity);
        ResponseEntity<Todo> response = todoService.createTask(todoEntity);
        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void get_failure_when_create_task_test() {
        ResponseEntity<Todo> response = todoService.createTask(fakeTodoEntity);
        assertEquals(response.getStatusCode(), HttpStatus.NOT_ACCEPTABLE);
    }

    @Test
    public void get_success_when__get_all_task_test() {
        List<Todo> list = new ArrayList<>();
        Todo todo1 = new Todo("Sleep", "high", "pending", 1);
        Todo todo2 = new Todo("Sleep again", "medium", "pending", 2);

        list.add(todo1);
        list.add(todo2);

        when(todoRepository.findAllByStatus(status)).thenReturn(list);
        ResponseEntity<Map<String, List<Todo>>> response = todoService.getAllTodoList(status);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void get_failure_when_get_all_task_test() {
        List<Todo> list = new ArrayList<>();
        when(todoRepository.findAllByStatus(status)).thenReturn(list);
        ResponseEntity<Map<String, List<Todo>>> response = todoService.getAllTodoList(status);
        Assert.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void get_success_when_update_task() {
        Todo todo = new Todo("sleep", "high", "pending", 1);
        when(todoRepository.findById(Long.valueOf(2))).thenReturn(java.util.Optional.of(todo));
        when(todoRepository.save(todo)).thenReturn(todo);
        ResponseEntity<Todo> response = todoService.updateTask(todoEntity, Long.valueOf(2));
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void get_failure_when_delete_task(){
        doNothing().when(todoRepository).deleteById(any());
        ResponseEntity<HttpStatus> response = todoService.deleteTask(Long.valueOf(2));
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
