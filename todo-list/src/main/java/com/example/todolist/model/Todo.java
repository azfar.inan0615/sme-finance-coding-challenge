package com.example.todolist.model;

import lombok.Builder;


import javax.persistence.*;

@Builder
@Entity
@Table(name = "todo")
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "task")
    private String task;

    @Column(name = "priority")
    private String priority;

    @Column(name = "status")
    private String status;

    @Column(name = "weight")
    private int weight;

    public  Todo(){}

    public Todo(String task, String priority, String status, int weight){
        this.task = task;
        this.priority = priority;
        this.status = status;
        this.weight = weight;
    }

    public Todo(Long id, String task, String priority, String status, int weight){
        this.id = id;
        this.task = task;
        this.priority = priority;
        this.status = status;
        this.weight = weight;
    }

    public Long getId() {
        return id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Todo List [id=" + id + ", task=" + task + ", priority=" + priority + ", status=" + status + "]";
    }
}
