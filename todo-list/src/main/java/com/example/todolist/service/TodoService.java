package com.example.todolist.service;

import com.example.todolist.model.Todo;
import com.example.todolist.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TodoService {

    @Autowired
    TodoRepository todoRepository;


    public ResponseEntity<Map<String, List<Todo>>> getAllTodoList(String status){
        try {
            List<Todo> todoList;
            if (status == null){
               todoList = new ArrayList<>(todoRepository.findAll(Sort.by("weight").ascending()));
            } else {
                todoList = new ArrayList<>(todoRepository.findAllByStatus(status));
            }

            if (todoList.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            Map<String, List<Todo>> map = new HashMap<>();
            map.put("data", todoList);

            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Todo> createTask(Todo todo){
        try{
            switch (todo.getPriority().toLowerCase(Locale.ROOT)) {
                case "high":
                    todo.setPriority("high");
                    todo.setWeight(1);
                    break;
                case "medium":
                    todo.setPriority("medium");
                    todo.setWeight(2);
                    break;
                case "low":
                    todo.setPriority("low");
                    todo.setWeight(3);
                    break;
                default:
                    return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
            }

            Todo _todo = todoRepository.save(new Todo(todo.getTask(), todo.getPriority(), "pending", todo.getWeight()));

            return new ResponseEntity<>(_todo, HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Todo> updateTask(Todo todo, Long id){
        Optional<Todo> todoData = todoRepository.findById(id);

        if (todoData.isPresent()) {
            Todo _todo = todoData.get();
            if(!todo.getTask().isEmpty()){
                _todo.setTask(todo.getTask());
            }

            if(!todo.getPriority().isEmpty()){
                switch (todo.getPriority().toLowerCase(Locale.ROOT)) {
                    case "high":
                        _todo.setWeight(1);
                        break;
                    case "medium":
                        _todo.setWeight(2);
                        break;
                    case "low":
                        _todo.setWeight(3);
                        break;
                }
                _todo.setPriority(todo.getPriority().toLowerCase(Locale.ROOT));
            }

            return new ResponseEntity<>(todoRepository.save(_todo), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<HttpStatus> deleteTask(long id) {
        try {
            todoRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Todo> completeTask(long id) {
        Optional<Todo> todoData = todoRepository.findById(id);

        if (todoData.isPresent()) {
            Todo _todo = todoData.get();
            _todo.setStatus("done");
            return new ResponseEntity<>(todoRepository.save(_todo), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Todo> getTaskById(long id) {
        Optional<Todo> todoData = todoRepository.findById(id);
        return todoData.map(todo -> new ResponseEntity<>(todo, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
