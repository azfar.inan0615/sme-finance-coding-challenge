package com.example.todolist.controller;

import com.example.todolist.model.Todo;
import com.example.todolist.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class TodoController {

    @Autowired
    TodoService todoService;

    @GetMapping("todo")
    public ResponseEntity<Map<String, List<Todo>>> getAllTodoList(@RequestParam(required = false) String status){
        return todoService.getAllTodoList(status);
    }

    @PostMapping("todo")
    public ResponseEntity<Todo> createTask(@RequestBody Todo todo){
        return todoService.createTask(todo);
    }

    @PutMapping("/todo/{id}")
    public ResponseEntity<Todo> updateTask(@PathVariable("id") long id, @RequestBody Todo todo) {
        return todoService.updateTask(todo, id);
    }

    @DeleteMapping("/todo/{id}")
    public ResponseEntity<HttpStatus> deleteTask(@PathVariable("id") long id) {
        return todoService.deleteTask(id);
    }

    @PutMapping("/todo/change-status/{id}")
    public ResponseEntity<Todo> completeTask(@PathVariable("id") long id) {
        return todoService.completeTask(id);
    }

    @GetMapping("todo/{id}")
    public ResponseEntity<Todo> getTaskById(@PathVariable("id") long id){
        return todoService.getTaskById(id);
    }
}
