package com.example.todolist.repository;

import com.example.todolist.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TodoRepository extends JpaRepository<Todo, Long> {
    @Query(value = "SELECT * FROM TODO t where t.status= ?1 Order by t.weight ASC", nativeQuery = true)
    List<Todo> findAllByStatus(String status);
}
